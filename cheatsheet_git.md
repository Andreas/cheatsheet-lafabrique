# Git : Les commandes de base

Voici une petite liste des commandes `git` de base qui vous permettront de créer
des commits et rendre votre travail.

Ce document ne constitue en aucun cas une documentation exhaustive de `git`, ni même un tutoriel.

Si vous cherchez cela, je vous conseille de lire l'excellent [Git book](https://git-scm.com/book/fr/v2) ou alors faire le [cursus openclassroom sur Git](https://openclassrooms.com/courses/gerez-vos-codes-source-avec-git)

## Qulques mots sur Git

[git](https://git-scm.com/) est un logiciel de gestion de version décentralisé. Grosso modo, c'est un outil qui permet aux développeurs de garder un historique des modifications faites dans le code et de pouvoir aisément travailler en équipe.

Il existe plusieurs outils de ce genre mais git est aujourd'hui le plus connu et sans doute le plus utilisé.

Si vous devez apprendre à utiliser un outil pour vous organiser dans votre code, c'est bien celui-ci !

Dans le milieu professionnel, quelques "fous" font encore à l'ancienne et partage du code par FTP, chat ou mail mais cela constitue une très mauvaise pratique.

Garder un historique des modifications et éviter ainsi les pertes de travail suite à des réécritures de fichiers par un tiers est le **B-A-B-A**.

Vous n'allez pas vous contenter de juste utiliser git pour rendre votre travail, vous allez bien l'utiliser et faire des commits réguliers avec des messages qui ont du sens !

Pour cela, des points spécifiques à git sont prévus dans le barême des projets.

## `git clone`

C'est souvent la commande qu'on utilise quand on commence un projet à partir
d'un projet GitHub ou GitLab (voir après) existant.

Elle créer un dossier avec le nom du projet (il est possible de préciser un nom
de dossier différent) dans lequel se trouvera les sources du projet ainsi que
son historique (contenu dans un dossier caché `.git`).

Sa syntaxe :

`git clone <adresse_git_du_projet>`

Exemple clonera dans un dossier `cheatsheet-lafabrique` :

`git clone git@framagit.org:Andreas/cheatsheet-lafabrique.git`

Autre exemple, clonera dans un dossier `autre-dossier` :

`git clone git@framagit.org:Andreas/cheatsheet-lafabrique.git autre-dossier`


## `git status`

Permet de savoir dans quel état se trouve son dossier `git`.

Si on veut savoir s'il y a des fichiers modifiés, supprimé, nouveaux, si on
diffère du serveur etc.

Cela permet aussi de savoir s'il y a des problèmes (merge ou autre).

Syntaxte :

`git status`

Parfois, `git status` affiche des fichiers "non suivi". Ce sont des fichiers (ou dossiers) présent dans le dossier géré par `git` mais dont l'historique n'est pas encore géré par `git`.

Vous pouvez faire un `git add` pour les rajouter ou alors les ignorer de git en les listant dans un fichier `.gitignore` que vous mettrez à la racine de votre dossier.

## `git add`

Vous permet d'ajouter un ou plusieurs fichier au commit que vous êtes en train
de créer. Permet aussi de rajouter des fichiers qui jusque là n'était pas suivi par `git`.

Syntaxe :

`git add <nom_du_fichier>`

Exemple :

`git add 2048.js`

Remarque, il est possible d'ajouter plusieurs fichiers à la fois en utilisants
des expressions régulières.

Exemple :

`git add *.js`

## `git rm` et `git mv`

Souvent, on veut supprimer ou modifier un fichier de notre projet.

On a l'habitude de le faire via une interface graphique, notre navigateur de
fichier généralement. C'est une mauvaise habitude quand on a un dossier géré par `git`, il vaut mieux laisser git faire la suppression ou le déplacement de fichier.

Le truc, c'est qu'il faut aussi dire à git que vous voulez le supprimer du
_dépôt git_.

On utilise alors la commande `git rm` pour supprimer un fichier :

`git rm <nom_du_fichier>`


Exemple :

`git rm index.html`

_Note : si `git rm` est appelé avant d'avoir supprimer le fichier sur le disque,
alors le fichier sera supprimé en même temps._

Et la commande `git mv` pour le déplacer :

`git mv <ancien_nom_de_fichier> <nouveau_nom_de_fichier>`

Exemple :

`git mv index.html 2048.html`

_Note : si vous avez oublié d'appeler `git mv` pour renommer ou déplacer un
fichier, pas de souci ! Vous pouvez faire un `git add` pour ajouter le fichier
que git considère comme nouveau. Et restez zen, git va retrouver ses petits et
garder l'historique sur le fichier._

## `git commit`

Une fois que vous avez ajouter tous les fichiers que vous voulez, vous pouvez
créer un _commit_.

Il s'agit de mettre un message sur un ensemble de modifications.

Plus le commit est petit et plus le message de commit est précis, mieux c'est !

N'hésitez pas à faire plusieurs commits par jour !

Dès que vous faites une petite modification dans votre code, faites un commit,
il n'y en a jamais trop (enfin faut pas abuser non plus).

Il est fortement conseillé d'écrire vos messages de commit en **anglais** !

_Note : J'utilise l'option `-m` pour préciser le message de commit directement
sur la ligne de commande. Si vous avez beaucoup de choses à dire, ne l'utilisez
pas, un éditeur de texte s'affichera et vous pourrez mettre autant de texte que
vous voudrez._

Ex :

`git commit -m "Add CSS classes on the grid"`

`git commit -m "Fix bug on login when no password"`

Evitez les messages trop évasifs genre :

`git commit -m "Update code"` ou `git commit -m "Fix bug"`

Tâchez de préciser au moins le nom du fichier impacté ou s'il y en a plusieurs,
la fonctionnalité.

Encore une fois, tâcher de séparer au maximum les commits. Pensez
fonctionnalités !

## Changer le dernier commit

Il est assez simple de changer le dernier commit en utilisant l'option :

`git commit --amend`

Au lieu de créer un nouveau commit, `git` ajoutera les nouvelles modifications
au commit précédent et vous permettra de modifier le message.

Pratique quand on s'est trompé ! Par contre, attention quand on travail à
plusieurs car le commit change de _hash_, il faudra donc faire un :

`git push -f`

Pour pouvoir envoyé votre commit sur le serveur et les autres personnes qui ont récupéré votre code risque d'être perdues.

A utilisé avec précaution donc, sauf si vous travaillé exclusivement en local.

## `git checkout`

C'est une commande qui peut être utilisée dans plusieurs cas (changement de
branche notamment) mais là on va l'utilser pour annuler des modifications sur un
fichier.

Si vous avez modifié un fichier, **sans l'avoir ajouté au commit**, mais que
finalement vous voulez **revenir à sa version initiale** (celle du dernier
commit) vous pouvez faire :

`git checkout <nom_du_fichier>`

Exemple : 

`git checkout css/styles.css`

Si vous voulez annulé l'ajout d'un fichier au commit courant, il faudra utiliser
la commande `git reset` décrire juste après.

## `git reset`

Parfois, on veut tout annuler, revenir à la version initiale d'un projet, `git
reset` est fait pour ça.

**Attention la commande qui suit réinitialise tout le projet par rapport à une
version précise. Toutes les modifications faites ultérieurement seront perdues**

`git reset --hard <nom_de_la_branche_ou_du_commit_ou_du_depot_distant>`

Exemple :

`git reset --hard master`

Quand on a modifié un fichier et qu'on l'a déjà ajouter au commit courant, si on
veut annuler l'ajout au commit (le "désindexé"), il faut faire un :

`git reset HEAD <nom_du_fichier>`

Exemple :

`git reset HEAD index.html`

Il ne vous restera plus qu'à faire un `git checkout <nom_du_fichier>` pour
annuler vos modification.

## `git log`

Cette commande vous montre l'ensemble des commits d'une branche avec leur hash
et les messages.

Très pratique quand on veut voir l'historique d'un projet.

`git log`

Pour avoir une version plus compacte des logs (et souvent bien suffisantes) faites un :

`git log --oneline`

Vous noterez que les hash des commits ne sont pas complets mais c'est pas bien grave car juste avec les 7 premiers caractères `git` assure qu'il n'y aura pas de collision en local.

## `git show`

Permet de voir le détail d'un commit qu'on a vu dans les logs.

`git show <hash_du_commit>`

On peut le faire avec des hash tronqués (visible dans un `git log --oneline) :

`git show c4352d5`

Ou avoir des hash complets :

`git show c4352d5def3c05d0f0855bc220d02f335e346dbf`

## `git pull`

Cette commande vous permet de récupérer les dernières modifications effectuées
sur le dépôt distant.

Si vous avez fait des modifications sur votre dépôt local, il faudra alors
_merger_ les deux dépôts et cela donne souvent la création d'un _commit de
merge_.

Syntaxe :

`git pull <nom_du_depot_distant> <nom_de_la_branche>`

Ex :

`git pull origin master`

Parfois, `git` ne peut pas merger automatiquement les sources, c'est souvent le
cas quand vous avez modifier un fichier qui a aussi été modifié dans le dépôt
distant.

Dans ce cas là, il faut faire le merge manuellement et c'est une autre pair de
manche !

Je vous conseille pour cela d'utiliser des outils comme `meld`

`sudo apt install meld`

Ensuite configurer git pour qu'il utilise `meld` :

`git config --global merge.tool meld`

Puis, vous pouvez faire un `git merge` après.

Je vous conseille de vous faire aider par quelqu'un d'expérimenté les premières
fois.

## `git push`

Une fois que vous aurez fait vos commits, récupérez les dernières sources du
serveur, gérer les éventuels conflits, vous pourrez publiez vos modification sur
le dépôt distant !

Syntaxe :

`git push <nom_du_depot_distant> <nom_de_la_branche>`

Ex :

`git push origin master`

## Et d'autres encore

Il existe beaucoup d'autres fonctions `git`, on a peu parlé des dépôts distants
et des branches, mais ça sera pour plus tard !

Avec ces quelques commandes, vous devrez déjà être plus ou moins autonome :).

### Sur Gitlab ton projet tu rendras

Si `git` est aussi connu c'est en grande partie grace au site [github](github.com), sorte de facebook pour développeurs open source.

Ce site a _radicalement_ changé la manière dont on fait du code. Que ce soit au niveau de la documentation, des commits, de la gestion de bugs, de la participation à un projet etc. Tout à changer !

Et en bien mieux ! Grâce à Github, il est aujourd'hui beaucoup plus facile de participer au développement communautaire d'un logiciel.

De plus, Github a instauré une sorte de "standard" qui profite à l'ensemble des développeurs.

C'est donc un site génial dont le seul défaut et de ne pas être basé sur un [logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre).

C'est pour cela que le projet [GitLab](https://about.gitlab.com/) a vu le jour et c'est pour cela qu'on utilisera pour notre projet une instance de Gitlab hébergée gracieusement par l'excellente association [framasoft](https://framasoft.org).

Afin d'utiliser framagit (et donc gitlab), réalisez les étapes suivantes :

* Créez-vous un compte sur [framagit](www.framagit.org)
* Ensuite "forker" le projet que vous voulez réaliser en cliquant sur le lien "fork" sur la page d'accueil du projet
* Cela va faire une copie du projet dans votre espace personnel
* Vous n'avez plus qu'à cloner le projet sur votre disque dur avec l'adresse fournit par framagit :
* Ex : `git clone git@framagit.org:<votre_nom>/2048-lafabrique.git`

Puis pour rendre votre projet vous n'aurez qu'à faire un `git push` comme décrit plus haut.
